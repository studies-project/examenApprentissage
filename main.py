import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import tree
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix

def analyzeData():
	data = pd.read_csv("exam.csv")
	print("Dimensions = ", data.shape, "\n")
	print("Infos = ", data.info(), "\n")
	print("Description = ", data.describe(), "\n")
	for element in data.columns:
		print("Nombre de ", element, " = ", data.value_counts(element), "\n")
	return data


# Avec la matrice de corrélation voit que F est très peu corrélée (-0.016) donc on peut la retirer
# pour voir si nous avons une meilleur précision. Le réseau de neurones ne devrait pas avoir de 
# grandes différences étant donné qu'il est capable de la supprimer automatiquement.
def splitData(data, test_ratio):
	# train : 2/3, test : 1/3
	train, test = train_test_split(data, test_size=1/3)
	del train['F'], test['F']
	xTrain = train
	yTrain = train['N']
	del xTrain['N']
	xTest = test
	yTest = test['N']
	del xTest['N']
	return xTrain, yTrain, xTest, yTest


# Enregistrement de la matrice de corrélation, l'affichage avec un plt.show() "mordait" sur l'affichage du réseau de neurones une fois sur deux.
def saveMatrix(data):
	plt.figure(figsize = (20,15))
	sns.heatmap(data.corr(), vmax=0.5, cmap="PiYG", annot=True)
	plt.savefig("correlationMatrix.png")


# Ici un très faible voisin ne semble pas faire de grande différence, 5 et 10 ont en moyenne une précision très similaire.
def trainKNN(data):
	xTrain, yTrain, xTest, yTest = splitData(data, 1/3)
	knn = KNeighborsClassifier(n_neighbors=5)
	knn.fit(xTrain, yTrain)
	yPred = knn.predict(xTest)
	print("Leaning with knn")
	print("Train score : ", knn.score(xTrain, yTrain), ", Test score : ", knn.score(xTest, yTest))
	print("Précision :", round(knn.score(xTest, yTest)*100, 2), "%")
	print(confusion_matrix(yTest, yPred))
	return round(knn.score(xTest, yTest)*100, 2)


# La profondeur de l'arbre ne semble pas non plus avoir une grande différence dans les résultats, 10 oui 15 n'a pas apporté des différence significatives.
def trainTree(data):
	xTrain, yTrain, xTest, yTest = splitData(data, 1/3)
	classifier = tree.DecisionTreeClassifier(criterion='gini', max_depth=5, min_impurity_decrease=0.02)
	classifier.fit(xTrain, yTrain)
	print("Leaning with decision tree")
	print("Train score : ", classifier.score(xTrain, yTrain), ", Test score : ", classifier.score(xTest, yTest))
	yPred = classifier.predict(xTest)
	print("Précision :", round(classifier.score(xTest, yTest)*100, 2), "%")
	print(confusion_matrix(yTest, yPred))
	return round(classifier.score(xTest, yTest)*100, 2)


# Changer le max_iter ne semble pas changer nos scores sur 1000 essais.
# Le random_state = 1 lui améliore grandement notre précision, passant en moyenne de 75% à 82%.
def trainMLPC(data):
	xTrain, yTrain, xTest, yTest = splitData(data, 1/3)
	clf = MLPClassifier(random_state=1, max_iter=1000).fit(xTrain, yTrain)
	yPred = clf.predict(xTest)
	print("Leaning with neural network")
	print("Train score :", clf.score(xTrain, yTrain), ", Test score : ", clf.score(xTest, yTest))
	print("Précision :", round(clf.score(xTest, yTest)*100, 2), "%")
	print(confusion_matrix(yTest, yPred))
	return round(clf.score(xTest, yTest)*100, 2)



def averageFunction(function, data):
	sum=0
	for x in range(1000):
		sum+=function(data)
	return round((sum/1000), 2)



def main():
	data = analyzeData()

	resultKnn=averageFunction(trainKNN, data)
	resultTree=averageFunction(trainTree, data)
	resultMLPC=averageFunction(trainMLPC, data)
	
	print("Moyenne sur 1000 fois pour knn : ", resultKnn, "%")
	print("Moyenne sur 1000 fois pour tree : ", resultTree, "%")
	print("Moyenne sur 1000 fois pour mlpc : ", resultMLPC, "%")

	saveMatrix(data)



main()
