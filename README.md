# Examen Apprentissage

Examen d'apprentissage en Master de l'ULCO.

## Installation

```bash
conda create -y -n apprentissageArtificiel python=3.8
conda activate apprentissageArtificiel
conda install -y numpy pandas seaborn matplotlib keras pydot graphviz
python3 main.py
```

## Résultats de tests scores après 1000 essais

### Avec F
- KNN 65.69 %
- Arbre : 76.34 %
- Réseaux de Neurones :
	- Avec le random_state à 1 :
		- Sur 1000 itérations : 79.25 %


### En retirant F
- KNN 65.61%
- Arbre : 76.63%
- Réseaux de Neurones : 
	- Avec le random_state à 0 : 
		- 75.25 %
	- Avec le random_state à 1 :
		- Sur 200 itérations (défaut) : 82.43%
		- Sur 1000 itérations : 82,50%

Avec ces résultats on peut donc estimer qu'en retirant F le réseau de neurones avec un random_state à 1 et à 1000 itérations aura la meilleure précision. Le nombre d'itération ne semble pas être le plus impactant.

## Remarques

- J'enregistre la matrice de corrélation car l'affichage avec un plt.show() "mordait" sur l'affichage du réseau de neurones une fois sur deux.

## Développeurs

- Ernesto Artigas

## Reporter

- Fabien Teytaud